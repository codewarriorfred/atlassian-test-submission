import React, { Component } from "react";
import "./App.css";
import InformationPanel from "./InformationPanel";
import { Button, Grid } from "@material-ui/core";
import { browserHistory } from "@version/react-router-v3";
import lodash from "lodash";
import axios from "axios";

const invertDirection = {
  asc: "desc",
  desc: "asc"
};

class App extends Component {
  state = {
    spaceData: {
      fields: {
        title: "",
        description: ""
      },
      sys: {
        id: ""
      }
    },
    rowsEntries: [
      {
        title: "",
        summary: "",
        createdBy: "",
        updatedBy: "",
        lastUpdated: ""
      }
    ],

    rowsAssets: [
      {
        title: "",
        contentType: "",
        fileName: "",
        createdBy: "",
        updatedBy: "",
        lastUpdated: ""
      }
    ],
    sortingColumn: "",
    sortDirection: "desc",
    userName: "",
    userId: "",
    allAvailableSpaces: []
  };

  componentDidMount() {
    // Getting the available spaces and setting the ui to use the first one.
    axios
      .get("/space")
      .then(function(response) {
        var allAvailableSpacesArray = [];
        // Loop through the spaces to render the available space buttons.
        for (var index = 0; index < response.items.length; ++index) {
          allAvailableSpacesArray[index] = {
            spaceName: response.items[index].fields.title,
            spaceId: response.items[index].sys.id
          };
        }

        this.setState({
          allAvailableSpaces: allAvailableSpacesArray,
          spaceData: response.items[0],
          userId: response.items[0].sys.createdBy
        });

        // Getting the user information for the first space with the user Id.
        axios
          .get("/user", {
            params: {
              ID: this.state.userId
            }
          })
          .then(function(response) {
            this.setState({
              userName: response.fields.name
            });
          })
          .catch(function(error) {
            console.log(error);
          });
      })
      .catch(function(error) {
        console.log(error);
      });

    // Using react-router to add the space id at the end of the URL.
    browserHistory.push("/" + this.state.spaceData.sys.id);

    this.getAssetsForSpace(this.state.spaceData.sys.id);
    this.getEntriesForSpace(this.state.spaceData.sys.id);
  }

  sort_array_by_key(array, key, sortDirection) {
    if (sortDirection === "asc") {
      return array.sort(function(a, b) {
        var x = a[key];
        var y = b[key];
        return x < y ? -1 : x > y ? 1 : 0;
      });
    } else {
      return array.sort(function(a, b) {
        var x = a[key];
        var y = b[key];
        return x > y ? -1 : x < y ? 1 : 0;
      });
    }
  }

  handleSort = columnName => {
    this.setState({
      sortingColumn: columnName
    });

    if (this.state.sortingColumn === columnName) {
      this.setState({
        sortDirection: invertDirection[this.state.sortDirection]
      });
    } else {
      this.setState({
        sortDirection: "asc"
      });
    }
  };

  handleSpaceChange = spaceId => {
    // Getting the available spaces and setting the ui to use the first one.
    axios
      .get("/space?spaceId=" + spaceId)
      .then(function(response) {
        this.setState({
          spaceData: response,
          userId: response.sys.createdBy
        });

        // Getting the user information for the space with the user Id.
        axios
          .get("/user", {
            params: {
              ID: this.state.userId
            }
          })
          .then(function(response) {
            this.setState({
              userName: response.fields.name
            });
          })
          .catch(function(error) {
            console.log(error);
          });
      })
      .catch(function(error) {
        console.log(error);
      });

    browserHistory.push("/" + this.state.spaceData.sys.id);

    this.getAssetsForSpace(this.state.spaceData.sys.id);
    this.getEntriesForSpace(this.state.spaceData.sys.id);
  };

  getAssetsForSpace(spaceId) {
    //Getting the assets for the space.
    axios
      .get("/user?spaceId=" + this.state.spaceData.sys.id + "/assets")
      .then(function(response) {
        var allAssetsArray = [];
        // Loop through the spaces to render the available space buttons.
        for (var index = 0; index < response.items.length; ++index) {
          allAssetsArray[index] = {
            title: response.items[index].fields.title,
            contentType: response.items[index].fields.contentType,
            fileName: response.items[index].fields.fileName,
            createdBy: this.state.userName,
            updatedBy: this.state.userName,
            lastUpdated: response.items[index].fields.updatedAt
          };
        }

        this.setState({
          rowsAssets: allAssetsArray
        });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  getEntriesForSpace(spaceId) {
    //Getting the entries for the space.
    axios
      .get("/user?spaceId=" + spaceId + "/entries")
      .then(function(response) {
        var allEntriesArray = [];
        // Loop through the spaces to render the available space buttons.
        for (var index = 0; index < response.items.length; ++index) {
          allEntriesArray[index] = {
            title: response.items[index].fields.title,
            summary: response.items[index].fields.summary,
            createdBy: this.state.userName,
            updatedBy: this.state.userName,
            lastUpdated: response.items[index].fields.updatedAt
          };
        }

        this.setState({
          rowsEntries: allEntriesArray
        });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  createDataEntries(title, summary, createdBy, updatedBy, lastUpdated) {
    return { title, summary, createdBy, updatedBy, lastUpdated };
  }

  createDataAssets(
    title,
    contentType,
    fileName,
    createdBy,
    updatedBy,
    lastUpdated
  ) {
    return {
      title,
      contentType,
      fileName,
      createdBy,
      updatedBy,
      lastUpdated
    };
  }

  render() {
    return (
      <div className="App">
        <Grid className="AppGrid" direction="row" container>
          <Grid
            item
            container
            className="LeftVerticalTab"
            direction="column"
            justify="flex-start"
            xs={2}
          >
            {this.state.allAvailableSpaces.map((space, key) => (
              <Button onClick={() => this.handleSpaceChange(space.spaceId)}>
                {space.spaceName}
              </Button>
            ))}
          </Grid>

          <Grid
            item
            direction="column"
            justify="flex-start"
            xs={10}
            className="MainScreen"
          >
            <InformationPanel
              name={this.state.userName}
              spaceName={this.state.spaceData.fields.title}
              spaceDescription={this.state.spaceData.fields.description}
              rowsEntries={this.sort_array_by_key(
                this.state.rowsEntries,
                lodash.camelCase(this.state.sortingColumn),
                this.state.sortDirection
              )}
              rowsAssets={this.sort_array_by_key(
                this.state.rowsAssets,
                lodash.camelCase(this.state.sortingColumn),
                this.state.sortDirection
              )}
              handleSort={this.handleSort}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default App;
