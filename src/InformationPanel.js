import React from "react";
import PropTypes from "prop-types";
import "./InformationPanel.css";
import {
	Tabs,
	Tab,
	Table,
	TableBody,
	TableCell,
	TableHead,
	TableRow
} from "@material-ui/core";

export default function InformationPanel(props) {
	const [value, setValue] = React.useState(0);

	const assetsRowNames = [
		"Title",
		"Content Type",
		"File Name",
		"Created By",
		"Updated By",
		"Last Updated"
	];

	const entriesRowNames = [
		"Title",
		"Summary",
		"Created By",
		"Updated By",
		"Last Updated"
	];

	function handleChange(event, newValue) {
		setValue(newValue);
	}

	return (
		<div className="InformationPanel">
			<div className="Title">
				<h1>{props.spaceName}</h1>
				<h3 className="CreatedBy">created by {props.name} </h3>
			</div>

			<div className="TextField">
				<p>{props.spaceDescription}</p>
			</div>

			<div className="Table">
				<Tabs value={value} onChange={handleChange}>
					<Tab label="Entries" />
					<Tab label="Assets"></Tab>
				</Tabs>
				{value === 0 && (
					<div className="EntriesTable">
						<Table>
							<TableHead>
								<TableRow>
									{entriesRowNames.map(rowName => (
										<TableCell
											align="right"
											onClick={() =>
												props.handleSort(rowName)
											}
										>
											{rowName}
										</TableCell>
									))}
								</TableRow>
							</TableHead>
							<TableBody>
								{props.rowsEntries.map(row => (
									<TableRow>
										<TableCell>{row.title}</TableCell>
										<TableCell align="right">
											{row.summary}
										</TableCell>
										<TableCell align="right">
											{row.createdBy}
										</TableCell>
										<TableCell align="right">
											{row.updatedBy}
										</TableCell>
										<TableCell align="right">
											{row.lastUpdated}
										</TableCell>
									</TableRow>
								))}
							</TableBody>
						</Table>
					</div>
				)}

				{value === 1 && (
					<div className="AssetsTable">
						<Table>
							<TableHead>
								<TableRow>
									{assetsRowNames.map(rowName => (
										<TableCell
											align="right"
											onClick={() =>
												props.handleSort(rowName)
											}
										>
											{rowName}
										</TableCell>
									))}
								</TableRow>
							</TableHead>
							<TableBody>
								{props.rowsAssets.map(row => (
									<TableRow>
										<TableCell align="right">
											{row.title}
										</TableCell>
										<TableCell align="right">
											{row.contentType}
										</TableCell>
										<TableCell align="right">
											{row.fileName}
										</TableCell>
										<TableCell align="right">
											{row.createdBy}
										</TableCell>
										<TableCell align="right">
											{row.updatedBy}
										</TableCell>
										<TableCell align="right">
											{row.lastUpdated}
										</TableCell>
									</TableRow>
								))}
							</TableBody>
						</Table>
					</div>
				)}
			</div>
		</div>
	);
}

InformationPanel.propTypes = {
	name: PropTypes.string,
	spaceName: PropTypes.string,
	spaceDescription: PropTypes.string,
	rowsEntries: PropTypes.array,
	rowsAssets: PropTypes.array,
	handleSort: PropTypes.func
};

InformationPanel.defaultProps = {
	name: "Frednick",
	spaceDescription: "Blah",
	rowsEntries: [],
	rowsAssets: []
};
