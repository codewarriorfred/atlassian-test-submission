This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm install`

To install all the npm packages used for this project.
Then run :

### `npm start`

To create the local server and access it via web browser.

Navigate to `localhost:3000` unless indicated differently in the terminal.
Note: This will only display an empty screen since the api that it is trying to hit does not exist. 
